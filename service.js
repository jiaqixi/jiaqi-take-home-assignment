var fs = require('fs');
var express = require('express');
var app = express();
var Twitter = require('twitter');

// Set up twitter configuration
var client = new Twitter({
  consumer_key: '15sWbOMzxmayQDzdD4y4tDPni',
  consumer_secret: 'mRmu9FYOomi8IYnrRpJGmgkoP9XfS12H980rGe3MSORES21Dsr',
  access_token_key: '778581459103719424-LP2iWuyMHqdGOWmeRpE5DHUFsFXHm28',
  access_token_secret: 'LiWBC1UdxXapAJTVvo89vjfF97rcm2rtIESKkYugI3XPO'
});

// Part 1
app.get('/', function(req, res) {
  res.send('Try /hello/:name');
});

// Part 2
app.get('/hello/:name', function(req,res) {
  var name = req.params.name;
  var content = 'Hello ' + name;
  res.send(content);

});

// Part 3
app.get('/histogram/:name', function(req,res) {
  var name = req.params.name;
  var options = {screen_name: name};
  var today = new Date().toLocaleDateString('ISO', {year: 'numeric', month: 'numeric', day: 'numeric'});
  var currentTime = new Date();
  var yesterdayTime = new Date(currentTime -(24*60*60*1000));
  var yesterday = yesterdayTime.toLocaleDateString('ISO', {year: 'numeric', month: 'numeric', day: 'numeric'});
  var options = {q:'from:'+name+' since:'+yesterday+' until:'+today};
  var twittersTime = [];
  var tweetPerHour;
  client.get('search/tweets', options, function(err,tweets, response) {
    if(tweets.statuses.length > 0) {
      for(var i = 0; i < tweets.statuses.length; i++) {
      var time = parseTwitterDate(tweets.statuses[i].created_at);
      twittersTime.push(time);
      }
      tweetPerHour = getTweetsNum(twittersTime, currentTime);
      res.send(tweetPerHour);
    } else {
      res.send('please input the exsiting name');
    }

  });


});

// Parse UTC time from Twitter's created_at field
function parseTwitterDate(date) {
  var time = new Date(Date.parse(date.replace(/( +)/, 'UTC$1')));
  return time;
}

// Get the number of tweets per Hour
// TweetPerHour['0'] means the number of tweets --> 0 hour ago
function getTweetsNum(twittersTime, currentTime) {
  var tweetPerHour = {'0': 0, '1': 0, '2': 0, '3':0, '4': 0, '5': 0,
                       '6': 0, '7': 0, '8': 0, '9': 0, '10': 0, '11': 0,
                       '12': 0, '13': 0, '14': 0, '15': 0, '16': 0, '17': 0,
                       '18': 0, '19': 0, '20': 0, '21': 0, '22': 0, '23': 0,
                      };
  // var timeGet = currentTime.getTime();
  var getTime = (new Date(currentTime)).getTime();
  for (var m = 0; m < twittersTime.length; m++) {
    var time = (new Date(twittersTime[m])).getTime();
    var diff = getTime - time;
    var minute = Math.floor(diff/60000);
    var hour = Math.floor(minute/60);

    tweetPerHour[hour]++;
  }

  return tweetPerHour;
}

app.listen(3000, function() {
  console.log('Jiaqi Take Home Assignment');

});

module.exports.getTweetsNum = getTweetsNum;