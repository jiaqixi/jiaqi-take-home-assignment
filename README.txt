SETUP 

1 Download nodeJS from its website
2 Download curl.exe  ---- I have already saved curl into TakeHome folder
3 Create TakeHome folder -- For me , I created the folder in D:\TakeHome
4 Create js file --- I created the file called service.js
5 Intall express web framework   
   1) In Node.js command prompt, change the direction into D:\TakeHome -- you can change into the place where you save the folder
   2)Run-> npm init     
      That creates the empty package.json
   3) Run -> npm intall express --save
    4) Run -> npm install twitter --save
       They can save the express and twittter API into node_modules folder

5) Run the service.js file 
  run -> node service.js
6) Goes to browers
when you type  http://localhost:3000/
http://localhost:3000/hello/world
http://localhost:3000/histogram/KimKardashian

7) When you use curl, 
Keep  running node service.js 
Open a command promp, type
curl http://localhost:3000/histogram/twitter

--------------------------------------
Unit Test

1 Goes to the folder where you store this file  -- Mine is D:\TakeHome
2 Install Mocha and Chai 

run -> npm install mocha --save
run -> npm install chai --save

When you run mocha test, please do not run -> node service.js
Also please do not  run http://3000... in browser

When you run mocha test,  please run -> mocha test.js  under the folder   --- Mine is D:\TakeHome


