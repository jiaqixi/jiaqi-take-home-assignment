'use strict';
const fs = require('fs');
const chai = require('chai');
const expect = require('chai').expect;
const should = require('chai').should();

var service = require('./service');

describe('Check if get the right number of twitter', function() {

  // Test data
  var currentTime = 'Fri Sep 23 2016 10:55:42 GMT+1000 (AUS Eastern Standard Time)';
  var twittersTime = ['Fri Sep 23 2016 06:33:51 GMT+1000 (AUS Eastern Standard Time)',
                      'Thu Sep 22 2016 10:55:50 GMT+1000 (AUS Eastern Standard Time)'];
  it('should get the right tweets numver for last 24 hours ago', function() {

    var result = service.getTweetsNum(twittersTime, currentTime);
    expect(result['23']).to.deep.equal(1);
  });
});